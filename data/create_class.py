import os
from PIL import Image
import numpy as np
import pickle

#this function load the image and resize it to the 128x128
def load_image(infilename):
    img = Image.open(infilename)
    img.load()
    img = img.resize((128, 128))
    #print(img.format, img.size, img.mode)
    # img.show()
    data = np.asarray(img, dtype="int32")
    return data


def getFullPath(dirname, filename):
    return os.path.join(dirname, filename)

#this function loads all the image for training to one numpy big matrix
def saveImageData(batch_name):
    tags = [f for f in os.listdir(batch_name)]
    tags.sort()
    count = 0
    tagname = []
    imagedata = []
    for tag in tags:
        filename = [f for f in os.listdir(getFullPath(batch_name, tag))]
        for file in filename:
            fullfilepath = getFullPath(getFullPath(batch_name, tag), file)
            data = load_image(fullfilepath)
            imagedata.append(data)
            tagname.append(count)
            # print data.shape
            # print data
        #print str(count) + "," + tag
        count = count + 1

    finalresult = {"data": imagedata, "id": tagname}
    with open(batch_name + ".bin", 'wb') as handle:
        pickle.dump(finalresult, handle, protocol=pickle.HIGHEST_PROTOCOL)

saveImageData("Training") #this saves the numpy matrix to binary format 
saveImageData("Testing")
