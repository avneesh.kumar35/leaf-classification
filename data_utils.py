import pickle
import numpy as np
import os


def load_Batch(filename):
    with open(filename, 'rb') as handle:
        final = pickle.load(handle)
    X = final['data']
    Y = final['id']
    X = np.asarray(X)
    Y = np.asarray(Y)
    return X, Y


def load_All(ROOT):
    f = os.path.join(ROOT, "Training.bin")
    Xtr, Ytr = load_Batch(f)
    f = os.path.join(ROOT, "Testing.bin")
    Xte, Yte = load_Batch(f)
    return Xtr, Ytr, Xte,Yte

#Xtr, Ytr, Xte, Yte = load_All("data")
#print Xtr.shape
#print Ytr.shape
#print Xte
#print Yte.shape




