Read line number 72 first
1. File Hierchy
-------Root/
	|
----------data/
	    |
--------------Testing/
		|
-------------------1.jpg 2.jpg
	    |
--------------Training/
		|
-------------------1.jpg 2.jpg
	    |
--------------Validation/
		|
-------------------1.jpg 2.jpg
	    |
--------------create_class.py
	    |
--------------TagName.txt
	    |
----------data_utils.py
	    |
----------precompute_layer.py
	    |
----------transfer_leaf_softmax.py
	    |
----------tnse.py
	    |
----------visualize.py
	    |
----------resources/
		|
-------------------classify_image_graph_def.pb
            |
----------requirements.txt
	    |
----------retrainedmodel/


$ pip install -r requirements.txt
$ cd $HOME/Root

Copy all the training image data to Training folder.
Copy all the testing image data to Testing folder.
Copy all the validation image data to Testing folder.

$ cd data
$ find . -name ".DS_Store" -delete
$ python create_class.py #this command will generate two files Testing.bin and Training.bin

$ cd ../
$ mkdir resources
Copy the classify_image_graph_def.pb to the resources folder

$ python precompute_layer.py 
the above command will take long time 1 hrs or more depending upon the size of the training dataset.
it will generate X_test.npy X_train.npy y_test.npy y_train.npy

$ mkdir retrainedmodel
$ python transfer_leaf_softmax.py
This command will save the retrained model in retrainedmodel folder



#for visualization
$ python visualize.py
$ tensorboard --logdir /tmp/inception_v3_log


Since all the pretrained models are in the folder. you can skip all the above steps. You just fill the data/Validation folder with the validation images
and run the step
$ python predictor.py
If you want to do step by step the follow from begining of the README.

If you look at the graph you can see all the cnv layers and mized layers.
Conv layers and mixed layer apply conv filters over the image and the main 
classification happens at the final pool3 layer.
I am adding images in report directory. Do have a look at the layers.
IF you want more detailed look at the CNN graph, you can use tensorboard as explained in the above comands

So what i did was transfer learning.
In transfer learning you basically train the final pool3 and softmax layer for the new categories which I did.
you can find the feeding function in precompute_layer and see that the batch_pool3_features functions.

Once I have the data saved in the above part, all the training happens in the transfer_leaf_softmax.py 
And the final trained graph is outputted in the retrained folder.

In the predictor.py if you look at the code it loads the image , resizxe it and feed it to the tensorflow to get the 
output class.



