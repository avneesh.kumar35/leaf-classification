from data_utils import load_All
import numpy as np
from datetime import datetime
import tensorflow as tf
from tensorflow.python.platform import gfile

model = 'resources/classify_image_graph_def.pb'

def create_graph():
    """"Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    print 'Loading graph...'
    with tf.Session() as sess:
        with gfile.FastGFile(model, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')
    return sess.graph


def batch_pool3_features(sess,X_input):
    """
    Currently tensorflow can't extract pool3 in batch so this is slow:
    https://github.com/tensorflow/tensorflow/issues/1021
    """
    n_train = X_input.shape[0]
    print 'Extracting features for %i rows' % n_train
    pool3 = sess.graph.get_tensor_by_name('pool_3:0')
    X_pool3 = []
    for i in range(n_train):
        print 'Iteration %i' % i
        print X_input[i,:].shape
        pool3_features = sess.run(pool3,{'DecodeJpeg:0': X_input[i,:]})
        X_pool3.append(np.squeeze(pool3_features))
    return np.array(X_pool3)





data_dir = 'data'

def serialize_cifar_pool3(X,filename):
    print 'About to generate file: %s' % filename
    sess = tf.InteractiveSession()
    create_graph()
    X_pool3 = batch_pool3_features(sess,X)
    np.save(filename,X_pool3)

def serialize_data():
    X_train, y_train, X_test, y_test = load_All(data_dir)
    serialize_cifar_pool3(X_train, 'X_train')
    serialize_cifar_pool3(X_test, 'X_test')
    np.save('y_train',y_train)
    np.save('y_test',y_test)

#print classes.shape
serialize_data()
