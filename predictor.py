import tensorflow as tf
import numpy as np
import operator
import os
from PIL import Image


def getTags():
    lines = []
    with open("data/TagName.txt") as f:
        content = f.readlines()
    content = [x.strip().split(",")[1] for x in content]
    return np.asarray(content)


def ensure_name_has_port(tensor_name):
    """Makes sure that there's a port number at the end of the tensor name.
    Args:
      tensor_name: A string representing the name of a tensor in a graph.
    Returns:
      The input string with a :0 appended if no port was specified.
    """
    if ':' not in tensor_name:
        name_with_port = tensor_name + ':0'
    else:
        name_with_port = tensor_name
    return name_with_port

classes = getTags()


FLAGS = tf.app.flags.FLAGS
BOTTLENECK_TENSOR_SIZE = 2048
tf.app.flags.DEFINE_string('final_tensor_name', 'final_result',
                           """The name of the output classification layer in"""
                           """ the retrained graph.""")


def create_graph(sess):
    """Creates a graph from saved GraphDef file and returns a saver."""
    # Creates graph from saved graph_def.pb.
    new_saver = tf.train.import_meta_graph('retrainedmodel/my-model.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('retrainedmodel/'))
    return sess


def run_inference_on_image(image_path, sess):

    img = Image.open(image_path)
    img.load()
    img = img.resize((128, 128))
    data = np.asarray(img, dtype="float32")
    softmax_tensor = sess.graph.get_tensor_by_name('pool_3:0')
    pool3_features = sess.run(softmax_tensor, {'DecodeJpeg:0': data[:]})
    pool3_features = pool3_features.reshape(1, BOTTLENECK_TENSOR_SIZE)
    result_tensor = sess.graph.get_tensor_by_name(
        ensure_name_has_port(FLAGS.final_tensor_name))
    probs = sess.run(result_tensor, feed_dict={
                     "X_Bottleneck:0": pool3_features})
    predicted_class = classes[np.argmax(probs)]
    return predicted_class


def getFullPath(dirname, filename):
    return os.path.join(dirname, filename)

def CheckNetonValidation():
    correctcount = 0
    totalcount = 0
    wrongcount = 0
    with tf.Session() as sess:
        sess = create_graph(sess)
        image = "data/Validation/acer_negundo/13001151167202.jpg"
        tags = [f for f in os.listdir("data/Validation/")]
        tags.sort()
        for tag in tags:
            filename = [f for f in os.listdir(
                getFullPath("data/Validation/", tag))]
            for file in filename:
                fullfilepath = getFullPath(
                    getFullPath("data/Validation/", tag), file)
                predictedclass = run_inference_on_image(fullfilepath, sess)
                if predictedclass == tag:
                    correctcount = correctcount + 1
                else:
                    wrongcount = wrongcount + 1
                totalcount = totalcount + 1
    print "totalcount of validation dataset image files"
    print totalcount
    print "Correct prediction of validation dataset image files"
    print correctcount
    print "Wrong Prediction count of validation dataset image files"
    print wrongcount
    print "\n"

CheckNetonValidation()



