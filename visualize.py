import os
import os.path
import tensorflow as tf
from tensorflow.python.platform import gfile

INCEPTION_LOG_DIR = '/tmp/inception_v3_log'

if not os.path.exists(INCEPTION_LOG_DIR):
    os.makedirs(INCEPTION_LOG_DIR)
with tf.Session() as sess:
    new_saver = tf.train.import_meta_graph('retrainedmodel/my-model.meta')
    new_saver.restore(sess, tf.train.latest_checkpoint('retrainedmodel/'))
    writer = tf.summary.FileWriter(INCEPTION_LOG_DIR, sess.graph)
    writer.close()
